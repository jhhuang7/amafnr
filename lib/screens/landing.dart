import "package:flutter/material.dart";
import "package:shared_preferences/shared_preferences.dart";

import "package:amafnr/screens/home.dart";

class Landing extends StatefulWidget {
  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {
  String country;
  String topic;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      country = prefs.getString("country");
      topic = prefs.getString("topic");
    });
  }

  @override
  Widget build(BuildContext context) {
    if (country != null && topic != null) {
      return Home();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("amafnr"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Image.asset(
                "assets/images/icon.png",
              height: 200,
              width: 200,
            ),
            Padding(
              padding: const EdgeInsets.all(25),
              child: Text(
                  "Welcome to amafnr: A Mobile App For News Recommendation! "
                      "We understand that life as a uni student is "
                      "busy and it’s "
                      "difficult to actively look for news "
                      "content specific to you. "
                      "This is where this handy little app comes in; "
                      "amafnr aims to provide you with the "
                      "latest trending news "
                      "items along with content tailored to your interests! "
                      "To protect your privacy, all data collected from the "
                      "app is stored locally on the mobile device and will be "
                      "erased as soon as you log out of the app; "
                      "there is no central database to spread, sell "
                      "or manipulate your data. "
                      "Enter some basic details and "
                      "get informed now!",
                textAlign: TextAlign.center,
                textScaleFactor: 1.2,
                )
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(width: 200, child: ElevatedButton(
                      onPressed: () => {
                        Navigator.pushNamed(context, "/signup")
                      },
                      child: Text("Enter App"),
                      style: ButtonStyle(
                        backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.yellow),
                        foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.black)
                      ),
                  )),
                ]
            )
          ],
        ),
      )
    );
  }
}

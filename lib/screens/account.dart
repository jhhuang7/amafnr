import "package:flutter/material.dart";
import "package:fluttertoast/fluttertoast.dart";
import "package:shared_preferences/shared_preferences.dart";
import "package:validators/validators.dart" as val;
import "package:dots_indicator/dots_indicator.dart";

import "package:amafnr/models/interests.dart";

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  Map<String, Widget> interests = {};

  final interestController = TextEditingController();
  final countryController = TextEditingController();
  final topicController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _itemBuilder();
    getData();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    interestController.dispose();
    countryController.dispose();
    topicController.dispose();
    super.dispose();
  }

  void getData() async {
    // Get account data and pre-fill from local storage
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      countryController.text = prefs.getString("country");
      topicController.text = prefs.getString("topic");
    });
  }

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);

    return Scaffold(
      appBar: AppBar(
        title: Text("Account"),
        actions: [
          GestureDetector(
            onTap: () => {
              _showDialog(context)
            },
            child: Row(
                children: [
                  Text("Logout"),
                  Icon(Icons.logout)
              ]
            )
          ),
          VerticalDivider(color: Colors.transparent)
        ]
      ),
      body: PageView(
        scrollDirection: Axis.horizontal,
        controller: controller,
        children: <Widget>[
          personalPage(controller),
          interestsPage(controller)
        ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "For You"
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.trending_up),
            label: "Trending"
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: "Account"
          ),
        ],
        selectedItemColor: Colors.purpleAccent,
        currentIndex: 2,
        onTap: (index) {
          Navigator.popUntil(context,ModalRoute.withName("/"));

          if (index == 0) {
            Navigator.pushNamed(context, "/home");
          } else if (index == 1) {
            Navigator.pushNamed(context, "/trending");
          } else {
            Navigator.pushNamed(context, "/account");
          }
        }
      )
    );
  }

  Future<void> _showDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Exit"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    "Are you sure you want to exit the app? "
                        "All saved information will be lost."
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              }
            ),
            TextButton(
              child: Text("Yes"),
              onPressed: () async {
                // Clear everything
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove("country");
                prefs.remove("topic");

                // Remove interests
                await Interests.clearInterests();

                Navigator.popUntil(context,ModalRoute.withName("/"));
                Navigator.pushNamed(context, "/");
              }
            )
          ]
        );
      },
    );
  }

  Widget personalPage(PageController controller) {
    // Pulled info from DB and fill in text boxes

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 150, child: ElevatedButton(
                    onPressed: () => {
                      controller.jumpToPage(0)
                    },
                    child: Text("Preferences"),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(
                            Colors.deepPurpleAccent
                        )
                    )
                )),
                SizedBox(width: 150, child: ElevatedButton(
                    onPressed: () => {
                      controller.jumpToPage(1)
                    },
                    child: Text("Interests"),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueGrey)
                    )
                ))
              ]
          ),
          DotsIndicator(
              dotsCount: 2,
              position: 0,
              decorator: DotsDecorator(
                  color: Colors.blueGrey,
                  activeColor: Colors.deepPurpleAccent
              ),
              onTap: (position) {
                controller.jumpToPage(position.toInt());
              }
          ),
          Padding(
              padding: const EdgeInsets.all(25),
              child: DropdownButton<String>(
                value: countryController.text,
                icon: Icon(Icons.flag),
                isExpanded: true,
                onChanged: (country) {
                  setState(() {
                    countryController.text = country;
                  });
                },
                items: <String>[
                  "--Country--",
                  "Australia",
                  "Brazil",
                  "Canada",
                  "Switzerland",
                  "China",
                  "Germany",
                  "Egypt",
                  "Spain",
                  "France",
                  "United Kingdom",
                  "Greece",
                  "Hong Kong",
                  "Ireland",
                  "Israel",
                  "India",
                  "Italy",
                  "Japan",
                  "Netherlands",
                  "Norway",
                  "Peru",
                  "Philippines",
                  "Pakistan",
                  "Portugal",
                  "Romania",
                  "Russian Federation",
                  "Sweden",
                  "Singapore",
                  "Taiwan, Province of China",
                  "Ukraine",
                  "United States"
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList()
              )
          ),
          Padding(
              padding: const EdgeInsets.all(25),
              child: DropdownButton<String>(
                value: topicController.text,
                icon: Icon(Icons.fiber_new_sharp),
                isExpanded: true,
                onChanged: (topic) {
                  setState(() {
                    topicController.text = topic;
                  });
                },
                items: <String>[
                  "--News Preference--",
                  "world",
                  "nation",
                  "business",
                  "technology",
                  "entertainment",
                  "sports",
                  "science",
                  "health"
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList()
              )
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 200, child: ElevatedButton(
                    onPressed: () => {
                      // Check info and update
                      updatePersonal()
                    },
                    child: Text("Save"),
                    style: ButtonStyle(
                        backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.deepPurple)
                    )
                ))
              ]
          )
        ],
      ),
    );
  }

  void updatePersonal() async {
    final country = countryController.text.toString().trim();
    final topic = topicController.text.toString().trim();

    // Perform checks
    if (country != "--Country--" && topic != "--News Preference--") {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("country", country);
      prefs.setString("topic", topic);

      Fluttertoast.showToast(
          msg: "Your preferences have been updated.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );

      return;
    }

    Fluttertoast.showToast(
        msg: "Please try again, ensuring all fields are complete.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  Widget interestsPage(PageController controller) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 150, child: ElevatedButton(
                    onPressed: () => {
                      controller.jumpToPage(0)
                    },
                    child: Text("Preferences"),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueGrey)
                    )
                )),
                SizedBox(width: 150, child: ElevatedButton(
                    onPressed: () => {
                      controller.jumpToPage(1)
                    },
                    child: Text("Interests"),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(
                            Colors.deepPurpleAccent
                        )
                    )
                ))
              ]
          ),
          DotsIndicator(
            dotsCount: 2,
            position: 1,
              decorator: DotsDecorator(
                color: Colors.blueGrey,
                activeColor: Colors.deepPurpleAccent
              ),
              onTap: (position) {
                controller.jumpToPage(position.toInt());
              }
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        width: 200.0,
                        child: TextFormField(
                            decoration: InputDecoration(
                                border: UnderlineInputBorder(),
                                hintText: "Add Interest"
                            ),
                          controller: interestController,
                        )
                    ),
                    GestureDetector(
                      onTap: () async => {
                        addInterest()
                      },
                      child: Icon(
                          Icons.add_circle,
                          size: 30,
                          color: Colors.green
                      )
                    )
                  ]
          )),
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: this.interests.values.toList()
          )
        ]
      )
    );
  }

  void addInterest() async {
    // Add new interest
    Interests interest = new Interests(
        topic: interestController.text.trim().toLowerCase(),
        count: 1
    );

    if (interest.topic.length == 0 ||
        (!val.isAlphanumeric(interest.topic) &&
            !interest.topic.contains(" "))) {
      Fluttertoast.showToast(
          msg: "Bad interest add, please try again.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );
      interestController.clear();
      return;
    }

    // Check if the interest is somewhere in list already
    bool exists = false;
    for (int i = 0; i < this.interests.length; i++) {
      if (this.interests.keys.elementAt(i).contains(interest.topic) ||
          interest.topic.contains(interests.keys.elementAt(i))) {
        exists = true;
        break;
      }
    }

    if (exists) {
      Fluttertoast.showToast(
          msg: "Can't add duplicate interest.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );
    } else {
      setState(() {
        this.interests.addAll({
          interest.topic: interestItem(interest)
        });
      });

      await Interests.insertInterest(interest);
    }
    interestController.clear();
  }

  Widget interestItem(Interests interest) {
    Widget item =  Padding(
        padding: const EdgeInsets.all(15),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                interest.topic,
                textScaleFactor: 2,
                textAlign: TextAlign.left,
              ),
              GestureDetector(
                  onTap: () async => {
                    setState(() {
                      this.interests.remove(interest.topic);
                    }),
                    // Remove this interest from DB
                    await Interests.deleteInterest(interest.topic)
                  },
                  child: Icon(
                      Icons.delete_forever,
                      size: 30,
                      color: Colors.red
                  )
              )
            ]
        ));

    return item;
  }

  void _itemBuilder() async {
    List<Interests> items = await Interests.getInterests();

    setState(() {
      for (int i = 0; i < items.length; i++) {
        this.interests.addAll({items[i].topic: interestItem(items[i])});
      }
    });
  }
}

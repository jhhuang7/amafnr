import "package:flutter/material.dart";
import "package:validators/validators.dart" as val;

import "package:amafnr/models/news.dart";
import "package:amafnr/models/interests.dart";

class Trending extends StatefulWidget {

  @override
  _TrendingState createState() => _TrendingState();
}

class _TrendingState extends State<Trending> {
  Future<List<String>> futureArticles;

  @override
  void initState() {
    super.initState();

    // Trending page will always showing breaking-news in selected country
    futureArticles = News.fetchNews(["trending"]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Trending"),
      ),
      body: Center(
        child: FutureBuilder<List<String>>(
            future: futureArticles,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // API has max 10 articles per request, so for trending we'll
                // keep it to 10 articles to reduce cognitive overload.
                // Shouldn't be any duplicates in this case.
                List<News> items = News.getTop(
                    snapshot.data[0].toString(), 10
                );

                /*
                Error check for if items has 0 length
                If country is Spain API doesn't like this
                Or API calls exceeded daily limit
                */
                if (items.length == 0) {
                  return Padding(
                      padding: const EdgeInsets.all(25),
                      child: Text("Couldn't find any news articles, "
                          "please change your selected country and try again!",
                        textAlign: TextAlign.center
                  ));
                }

                return RefreshIndicator(
                    backgroundColor: Colors.grey,
                    onRefresh: () async {
                      Navigator.popUntil(context,ModalRoute.withName("/"));
                      Navigator.pushNamed(context, "/trending");
                    },
                    child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (context, index) =>
                          _itemBuilder(context, items[index], index)
                    )
                );
              } else if (snapshot.hasError) {
                // In very unlikely case there's some error
                return Padding(
                    padding: const EdgeInsets.all(25),
                    child: Text(
                    "There was an error loading news articles, "
                        "please click the 'Trending' icon to try again!",
                        textAlign: TextAlign.center
                    )
                );
              }
              // By default, show a loading spinner.
              return CircularProgressIndicator();
            }
        )
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "For You",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.trending_up),
            label: "Trending",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: "Account",
          ),
        ],
        selectedItemColor: Colors.purpleAccent,
        currentIndex: 1,
          onTap: (index) {
            Navigator.popUntil(context,ModalRoute.withName("/"));

            if (index == 0) {
              Navigator.pushNamed(context, "/home");
            } else if (index == 1) {
              Navigator.pushNamed(context, "/trending");
            } else {
              Navigator.pushNamed(context, "/account");
            }
          }
      )
    );
  }

  void logClick(News news) async {
    List<String> headline = news.title.split(" ");

    List<Interests> items = await Interests.getInterests();
    List<String> interests = [];
    for (Interests item in items) {
      interests.add(item.topic);
    }

    for (String word in headline) {
      String topic = word.toLowerCase();

      // If topic already somewhere in DB update its weight
      int index = interestExists(topic, interests);
      if (index != -1) {
        Interests.updateInterest(
            new Interests(
                topic: items[index].topic,
                count: items[index].count + 1
            )
        );
        continue;
      }

      // If topic isn't in DB and fits criteria then add it.
      // We want topic be be proper noun and have at least 7 character (magic)
      if (val.isAlphanumeric(topic) && val.isUppercase(word[0])
          && topic.length >= 7) {
        Interests.insertInterest(
            new Interests(
                topic: topic,
                count: 1
            )
        );
      }
    }
  }

  int interestExists(String topic, List<String> interests) {
    for (int i = 0; i < interests.length; i++) {
      // Check if topic already somewhere in list
      if (interests[i].contains(topic) || topic.contains(interests[i])) {
        return i;
      }
    }

    return -1;
  }

  Widget _itemBuilder(BuildContext context, News news, int index) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        elevation: 10,
        color: Colors.white70,
        child: GestureDetector(
            onTap: () => {
              // Log the click as user's new interest
              logClick(news),

              Navigator.pushNamed(
                  context, "/article", arguments: {"url": news.url}
                  )
            },
            child: Container(
                color: Colors.white70,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.network(
                          news.image,
                          fit: BoxFit.fill
                      ),
                      Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(
                            news.title,
                              textScaleFactor: 1.5,
                              textAlign: TextAlign.start
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text(
                              news.description,
                              textAlign: TextAlign.start
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                              news.source,
                              style: TextStyle(
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.center
                          )
                      )
                    ]
                )
            )
        )
    );
  }
}

import "package:flutter/material.dart";
import "package:shared_preferences/shared_preferences.dart";
import "package:validators/validators.dart" as val;

import "package:amafnr/models/news.dart";
import "package:amafnr/models/interests.dart";

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future<List<String>> futureArticles;
  List<Interests> items;
  List<String> topics = [];

  @override
  void initState() {
    super.initState();

    // Query DB and for each interest do 1 API call to produce a list
    personaliseArticles();
  }

  void personaliseArticles() async {
    items = await Interests.getInterests();

    // Use for initial tailored search when there are no interests
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String topic = prefs.getString("topic");

    for (Interests item in items) {
      setState(() {
        topics.add(item.topic);
      });
    }

    // Add user's news preference for further search
    setState(() {
      topics.add(topic.replaceAll("-", " "));
    });

    futureArticles = News.fetchNews(topics);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("For You"),
        ),
        body: Center(
            child: FutureBuilder<List<String>>(
                future: futureArticles,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    /*
                    For each interest get the `count` number of articles then
                    add to big list
                    */
                    List<News> news = [];
                    for (int i = 0; i < items.length; i++) {
                      news.addAll(
                          News.getTop(
                              snapshot.data[i].toString(),
                              items[i].count
                          )
                      );
                    }
                    // Add extra articles:
                    // 10 articles for news preference if no interests, else 2
                    int extras = 2;
                    if (news.length < 10) {
                      extras = 10;
                    }
                    news.addAll(
                      News.getTop(
                          snapshot.data[items.length].toString(),
                          extras
                      )
                    );

                    // Remove duplicates
                    var map = <String>{};
                    var set = <News>[];
                    for (News article in news) {
                      if (map.add(article.title)) {
                        set.add(article);
                      }
                    }

                    /*
                    Error check in rare case of 0 news articles
                    Or API calls exceeded daily limit
                    */
                    if (set.length == 0) {
                      return Padding(
                          padding: const EdgeInsets.all(25),
                          child: Text(
                          "Couldn't find any news articles, please check your "
                              "list of interests and try again!",
                              textAlign: TextAlign.center
                      ));
                    }

                    return RefreshIndicator(
                        backgroundColor: Colors.grey,
                        onRefresh: () async {
                          Navigator.popUntil(context,ModalRoute.withName("/"));
                          Navigator.pushNamed(context, "/home");
                        },
                        child: ListView.builder(
                          itemCount: set.length,
                          itemBuilder: (context, index) =>
                              _itemBuilder(context, set[index], index)
                        )
                    );
                  } else if (snapshot.hasError) {
                    // In very unlikely case there's some error
                    return Padding(
                        padding: const EdgeInsets.all(25),
                        child: Text(
                        "There was an error loading news articles, "
                            "please click the 'For You' icon to try again!",
                            textAlign: TextAlign.center
                    ));
                  }
                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                }
            )
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: "For You",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.trending_up),
                label: "Trending",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                label: "Account",
              ),
            ],
            selectedItemColor: Colors.purpleAccent,
            currentIndex: 0,
            onTap: (index) {
              Navigator.popUntil(context,ModalRoute.withName("/"));

              if (index == 0) {
                Navigator.pushNamed(context, "/home");
              } else if (index == 1) {
                Navigator.pushNamed(context, "/trending");
              } else {
                Navigator.pushNamed(context, "/account");
              }
            }
        )
    );
  }

  void logClick(News news) async {
    List<String> headline = news.title.split(" ");

    List<Interests> items = await Interests.getInterests();
    List<String> interests = [];
    for (Interests item in items) {
      interests.add(item.topic);
    }

    for (String word in headline) {
      String topic = word.toLowerCase();

      // If topic already somewhere in DB update its weight
      int index = interestExists(topic, interests);
      if (index != -1) {
        Interests.updateInterest(
            new Interests(
                topic: items[index].topic,
                count: items[index].count + 1
            )
        );
        continue;
      }

      // If topic isn't in DB and fits criteria then add it.
      // We want topic be be proper noun and have at least 7 character (magic)
      if (val.isAlphanumeric(topic) && val.isUppercase(word[0])
          && topic.length >= 7) {
        Interests.insertInterest(
            new Interests(
                topic: topic,
                count: 1
            )
        );
      }
    }
  }

  int interestExists(String topic, List<String> interests) {
    for (int i = 0; i < interests.length; i++) {
      // Check if topic already somewhere in list
      if (interests[i].contains(topic) || topic.contains(interests[i])) {
        return i;
      }
    }

    return -1;
  }

  Widget _itemBuilder(BuildContext context, News news, int index) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        elevation: 10,
        color: Colors.white70,
        child: GestureDetector(
            onTap: () => {
              // Log the click as user's new interest
              logClick(news),

              Navigator.pushNamed(
                  context, "/article", arguments: {"url": news.url}
              )
            },
            child: Container(
                color: Colors.white70,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.network(
                          news.image,
                          fit: BoxFit.fill
                      ),
                      Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(
                              news.title,
                              textScaleFactor: 1.5,
                              textAlign: TextAlign.start
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text(
                              news.description,
                              textAlign: TextAlign.start
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                              news.source,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.center
                          )
                      )
                    ]
                )
            )
        )
    );
  }
}

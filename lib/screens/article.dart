import "package:flutter/material.dart";
import "package:webview_flutter/webview_flutter.dart";
import "package:flutter_share/flutter_share.dart";

class Article extends StatelessWidget {
  final String url;

  Article(this.url);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Article"),
        actions: [
          GestureDetector(
            onTap: () async => {
              await FlutterShare.share(
                  title: "Check out this interesting news article:",
                  linkUrl: this.url
              )
            },
            child: Row(
              children: [
                Text("Share"),
                Icon(Icons.share)
              ]
            )
          ),
          VerticalDivider(color: Colors.transparent)
        ]
      ),
      body: WebView(
          initialUrl: this.url,
          javascriptMode: JavascriptMode.unrestricted,
          onProgress: (int progress) {
            return CircularProgressIndicator();
          }
      )
    );
  }
}

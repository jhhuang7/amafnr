import "package:flutter/material.dart";
import "package:shared_preferences/shared_preferences.dart";
import "package:fluttertoast/fluttertoast.dart";

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final countryController = TextEditingController();
  final topicController = TextEditingController();

  @override
  void initState() {
    super.initState();
    countryController.text = "--Country--";
    topicController.text = "--News Preference--";
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    countryController.dispose();
    topicController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Enter App"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/icon.png",
              height: 200,
              width: 200,
            ),
            Padding(
                padding: const EdgeInsets.all(25),
                child: DropdownButton<String>(
                  value: countryController.text,
                  icon: Icon(Icons.flag),
                  isExpanded: true,
                  onChanged: (country) {
                    setState(() {
                      countryController.text = country;
                    });
                  },
                  items: <String>[
                    "--Country--",
                    "Australia",
                    "Brazil",
                    "Canada",
                    "Switzerland",
                    "China",
                    "Germany",
                    "Egypt",
                    "Spain",
                    "France",
                    "United Kingdom",
                    "Greece",
                    "Hong Kong",
                    "Ireland",
                    "Israel",
                    "India",
                    "Italy",
                    "Japan",
                    "Netherlands",
                    "Norway",
                    "Peru",
                    "Philippines",
                    "Pakistan",
                    "Portugal",
                    "Romania",
                    "Russian Federation",
                    "Sweden",
                    "Singapore",
                    "Taiwan, Province of China",
                    "Ukraine",
                    "United States"
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )
            ),
            Padding(
              padding: const EdgeInsets.all(25),
              child: DropdownButton<String>(
                value: topicController.text,
                icon: Icon(Icons.fiber_new_sharp),
                isExpanded: true,
                onChanged: (topic) {
                  setState(() {
                    topicController.text = topic;
                  });
                },
                items: <String>[
                  "--News Preference--",
                  "world",
                  "nation",
                  "business",
                  "technology",
                  "entertainment",
                  "sports",
                  "science",
                  "health"
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              )
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 200, child: ElevatedButton(
                      onPressed: () => {
                        // Check details
                        signup()
                      },
                      child: Text("Go!"),
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue)
                      )
                  ))
                ]
            )
          ]
        )
      )
    );
  }

  void signup() async {
    final country = countryController.text.toString().trim();
    final topic = topicController.text.toString().trim();

    // Perform checks
    if (country != "--Country--" && topic != "--News Preference--") {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("country", country);
      prefs.setString("topic", topic);

      Navigator.popUntil(context,ModalRoute.withName("/"));
      Navigator.pushNamed(context, "/home");
      return;
    }

    Fluttertoast.showToast(
        msg: "Please try again, ensuring all fields are complete.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }
}

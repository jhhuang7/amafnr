import "package:http/http.dart" as http;
import "package:shared_preferences/shared_preferences.dart";

import "dart:async";
import "dart:convert";

class News {
  final String title;
  final String description;
  final String url;
  final String image;
  final String source;

  News(this.title, this.description, this.url, this.image, this.source);

  @override
  String toString() {
    return "News{title: $title, description: $description"
        "url: $url image: $image source: $source}";
  }

  static List<News> getTop(String info, int numArticles) {
    final articleJson = json.decode(info);
    List<News> items = [];

    try {
      for (int i = 0; i < numArticles; i++) {
        String title = articleJson["articles"][i]["title"];
        String description = articleJson["articles"][i]["description"];
        String url = articleJson["articles"][i]["url"];
        String image = articleJson["articles"][i]["image"];
        String source = articleJson["articles"][i]["source"]["name"];

        News item = new News(title, description, url, image, source);
        items.add(item);
      }
    } catch (error) {
      // If we don't get index out of range error, return instantly
      return items;
    }

    return items;
  }

  static Future<List<String>> fetchNews(List<String> newsTypes) async {
    Map countries = {
      "Australia": "au",
      "Brazil": "br",
      "Canada":	"ca",
      "Switzerland": "ch",
      "China": "cn",
      "Germany": "de",
      "Egypt": "eg",
      "Spain": "es",
      "France": "fr",
      "United Kingdom":	"gb",
      "Greece": "gr",
      "Hong Kong": "hk",
      "Ireland": "ie",
      "Israel": "il",
      "India": "in",
      "Italy": "it",
      "Japan": "jp",
      "Netherlands": "nl",
      "Norway":	"no",
      "Peru":	"pe",
      "Philippines": "ph",
      "Pakistan": "pk",
      "Portugal": "pt",
      "Romania": "ro",
      "Russian Federation": "ru",
      "Sweden": "se",
      "Singapore": "sg",
      "Taiwan, Province of China": "tw",
      "Ukraine": "ua",
      "United States": "us"
    };

    // Get local storage info more informed searched
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String country = prefs.getString("country");

    List<String> jsons = [] ;

    for (String newsType in newsTypes) {
      String url;

      if (newsType == "trending") {
        url = "https://gnews.io/api/v4/top-headlines?"
            "topic=breaking-news&"
            "country=" + countries[country] + "&"
            "token=71876320190bf0c3556c08f5cf86d9a5";
      } else {
        url = "https://gnews.io/api/v4/"
            "search?q=" + newsType + "&"
            "country=" + countries[country] + "&"
            "token=71876320190bf0c3556c08f5cf86d9a5";
      }

      http.Response response = await http.get(Uri.parse(url));
      jsons.add(response.body.toString());
    }

    return jsons;
  }
}

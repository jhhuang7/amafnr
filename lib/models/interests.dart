import "package:flutter/widgets.dart";
import "package:path/path.dart";
import "package:sqflite/sqflite.dart";

class Interests {
  final String topic;
  final int count;

  Interests({this.topic, this.count});

  static Future<List<Interests>> getInterests() async {
    // Connect to DB and pull data
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
        join(await getDatabasesPath(), "database.db"),
        // When the database is first created, create a table.
        onCreate: (db, version) {
          // Run the CREATE TABLE statement on the database.
          return db.execute(
            "CREATE TABLE "
            "interests(topic TEXT PRIMARY KEY, count INTEGER)",
          );
        },
        // Set the version. This executes the onCreate function and provides a
        // path to perform database upgrades and downgrades.
        version: 1,
    );

    // Get a reference to the database.
    final db = await database;

    // Query the table for all
    final List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM interests ORDER BY count DESC, topic ASC;"
    );

    // Convert the List<Map<String, dynamic> into a List<Interest>.
    return List.generate(maps.length, (i) {
      return Interests(
          topic: maps[i]["topic"],
          count: maps[i]["count"]
      );
    });
  }

  static Future<void> insertInterest(Interests interests) async {
    WidgetsFlutterBinding.ensureInitialized();

    // Open the database and store the reference.
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), "database.db"),
      // When the database is first created, create a table.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE "
              "interests(topic TEXT PRIMARY KEY, count INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    // Get a reference to the database.
    final db = await database;

    // Insert into the correct table. Specify the
    // `conflictAlgorithm` to use in case the same interest is inserted twice.
    // In this case, replace any previous data.
    await db.insert(
      "interests",
      {"topic": interests.topic, "count": interests.count},
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<void> updateInterest(Interests interests) async {
    // Open the database and store the reference.
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), "database.db"),
      // When the database is first created, create a table.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE "
              "interests(topic TEXT PRIMARY KEY, count INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    // Get a reference to the database.
    final db = await database;

    // Update
    await db.update(
      "interests",
      interests.toMap(),
      // Ensure that there's a matching topic.
      where: "topic = ?",
      // Pass the id as a whereArg to prevent SQL injection.
      whereArgs: [interests.topic],
    );
  }

  static Future<void> deleteInterest(String topic) async {
    // Open the database and store the reference.
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), "database.db"),
      // When the database is first created, create a table.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE "
              "interests(topic TEXT PRIMARY KEY, count INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    // Get a reference to the database.
    final db = await database;

    // Remove the interest from the database.
    await db.delete(
      "interests",
      // Use a `where` clause to delete
      where: "topic = ?",
      // Pass the topic as a whereArg to prevent SQL injection.
      whereArgs: [topic],
    );
  }

  static Future<void> clearInterests() async {
    // Open the database and store the reference.
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), "database.db"),
      // When the database is first created, create a table.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE "
              "interests(topic TEXT PRIMARY KEY, count INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    // Get a reference to the database.
    final db = await database;

    // Clear the table to start again
    await db.delete("interests");
  }

  Map<String, dynamic> toMap() {
    return {
      "topic": this.topic,
      "count": this.count
    };
  }

  @override
  String toString() {
    return "Interests{topic: $topic, count: $count}";
  }
}

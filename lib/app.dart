import "package:flutter/material.dart";

import "package:amafnr/screens/account.dart";
import "package:amafnr/screens/article.dart";
import "package:amafnr/screens/home.dart";
import "package:amafnr/screens/landing.dart";
import "package:amafnr/screens/signup.dart";
import "package:amafnr/screens/trending.dart";

class App extends StatelessWidget {
  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "amafnr",
      onGenerateRoute: _routes(),
      theme: ThemeData(
        // This is the theme of your application.
        primarySwatch: Colors.purple,

        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }

  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;

      switch (settings.name) {
        case "/":
          screen = Landing();
          break;
        case "/signup":
          screen = Signup();
          break;
        case "/home":
          screen = Home();
          break;
        case "/trending":
          screen = Trending();
          break;
        case "/article":
          screen = Article(arguments["url"]);
          break;
        case "/account":
          screen = Account();
          break;
        default:
          return null;
      }

      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}
